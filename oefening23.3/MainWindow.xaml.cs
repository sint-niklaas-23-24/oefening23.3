﻿using System.Windows;

namespace oefening23._3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Click_Toevoegen(object sender, RoutedEventArgs e)
        {
            try
            {
                txtResultaat.Text += txtAchternaam.Text + txtVoorrnaam.Text.PadLeft(20) + (txtVerdiensten.Text.Replace(".", ",") + "€").PadLeft(20) + Environment.NewLine;
                txtAchternaam.Text = string.Empty;
                txtVoorrnaam.Text = string.Empty;
                txtVerdiensten.Text = string.Empty;

            }
            catch
            {
                MessageBox.Show("Geef letters in!", "Melding", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }
    }
}